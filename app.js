const express = require('express');
const bodyParser = require('body-parser');
const request = require('request');


const app = express();

app.use(express.static("public"));

app.use(bodyParser.urlencoded({
    extended: true
}));



app.get("/", function (req, res) {
    res.sendFile(__dirname + "/signup.html");
});

app.post("/", function (req, res) {
    let fName = req.body.fName;
    let lName = req.body.lName;
    let email = req.body.email;
  
    let data = {
        members: [
            {
            email_address: email,
            status: "subscribed",
            merge_fields: {
                "FNAME": fName,
                "LNAME": lName,
            }
            }
        ]
    };

    let jsonData = JSON.stringify(data);

    let options = {
        url: "https://us7.api.mailchimp.com/3.0/lists/92cfed8248",
        method: "POST",
        headers: {
            "Authorization":"projectoneuniverseid 20afddfb7a5f84dfad28a77b24f4ba5d"
        },
        body: jsonData
    };
    request(options, function (error, response, body) {
        if (response.statusCode === 200) {
            res.sendFile(__dirname + "/success.html");
        } else {
            res.sendFile(__dirname + "/failure.html");
        }
    });
});

app.listen(process.env.PORT || 3000, function () {
    console.log("Success connect to server port: 3000");
});